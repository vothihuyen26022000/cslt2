﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QUANLYNH_KS
{
    public partial class quanlykhachsan : Form
    {
        SqlConnection con = new SqlConnection();
        public quanlykhachsan()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string ConnectionString = "Data Source=DESKTOP-1K4VPR0\\SQLEXPRESS;Initial Catalog=quanlykhachsan;Integrated Security=True";
            con.ConnectionString = ConnectionString;
            con.Open();
            loadDataToGridView();
        }
        private void loadDataToGridView()
        {
            string sql = "select*from tblPhong";
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);
            DataTable tabletblPhong = new DataTable();
            adp.Fill(tabletblPhong);
            dataGridView_KSNH.DataSource = tabletblPhong;
        }
        private void dataGridView_KSNH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMaPhong.Text = dataGridView_KSNH.CurrentRow.Cells["maphong"].Value.ToString();
            txtTenPhong.Text = dataGridView_KSNH.CurrentRow.Cells["tenphong"].Value.ToString();
            txtDonGia.Text = dataGridView_KSNH.CurrentRow.Cells["dongia"].Value.ToString();
            txtMaPhong.Enabled = false;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            txtDonGia.Text = "";
            txtMaPhong.Text = "";
            txtTenPhong.Text = "";
            txtMaPhong.Enabled = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string sql = "delete from tblPhong where maphong = '" + txtMaPhong.Text + "'";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            loadDataToGridView();

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtMaPhong.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập mã phòng");
                txtMaPhong.Focus();
                return;
            }
            if (txtTenPhong.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập tên phòng");
                txtTenPhong.Focus();
                return;
            }
            else
            {
                string sql = "insert into btlPhong values ('" + txtMaPhong.Text + "', '" +
                txtTenPhong.Text + "'";
                if (txtDonGia.Text != "")
                {
                    sql = sql + "," + txtDonGia.Text.Trim();
                }
                sql = sql + ")";
                MessageBox.Show(sql);
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                loadDataToGridView();
            }

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtTenPhong.Text == "")
            {
                MessageBox.Show("bạn chưa nhập tên phòng");
                txtTenPhong.Focus();
                return;
            }
            if (txtDonGia.Text == "")
            {
                MessageBox.Show("bạn chưa nhập đơn giá");
                txtDonGia.Focus();
                return;
            }
            else
            {

                string sql = "update qlkhachsan set tenphong=@tenphong, dongia=@dongia where maphong =@maphong";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("maphong", txtMaPhong.Text);
                cmd.Parameters.AddWithValue("tenphong", txtTenPhong.Text);
                cmd.Parameters.AddWithValue("dongia", txtDonGia.Text);
                cmd.ExecuteNonQuery();
                loadDataToGridView();
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            btnThem.Enabled = false;
            txtMaPhong.Text = "";
            txtTenPhong.Text = "";
            txtDonGia.Text = "";

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            con.Close();
            this.Close();

        }
    }

    }
